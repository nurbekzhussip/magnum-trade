export interface IAction<TPayload = any> {
  type: string;
  method: string;
  headers?: { [key: string]: any };
  callAPI?: string;
  payload?: TPayload;
  authorization?: boolean;
  successRedirect?: string;
  error?: {
    errorCode: string;
    errorDesc: string;
    // errors?: {"code": string, "field": string, "rejectedValue": string}[];
  };
  response?: any;
}
