import { IApp } from "../../ducks/app";
import { IAuth } from "../../ducks/auth";

export interface IReduxState {
  app: IApp;
  auth: IAuth;
}
