export const errorCodes: { [key: string]: string } = {
  Email: 'Неверный формат почты',
  EmailRequired: 'Введите почту',
  PasswordRequired: 'Введите пароль',
  FieldRequired: 'Это поле обязательно',
};
