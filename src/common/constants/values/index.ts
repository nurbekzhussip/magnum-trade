import { LanguageType } from '../../../models';

export const NAV_ITEMS = [
  {
    title: 'ТОРГИ',
    href: '/lots',
  },
  {
    title: 'ТАРИФЫ',
    href: '/',
  },
  {
    title: 'ДОКУМЕНТЫ',
    href: '/',
  },
  {
    title: 'ПРОДАЖИ',
    href: '/',
  },
];

export const NAV_ITEMS_AUTH = [
  {
    title: 'ТОРГИ',
    href: '/lots',
  },
  {
    title: 'УВЕДОМЛЕНИЯ',
    href: '/',
  },
  {
    title: 'ПОСТАВЩИКИ',
    href: '/provider-account',
  },
  {
    title: 'АНАЛИТИКА',
    href: '/',
  },
  {
    title: 'ПРОДАЖИ',
    href: '/',
  },
];

export const LANGUAGES: Array<LanguageType> = ['kz', 'ru'];
