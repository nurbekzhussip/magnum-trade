// eslint-disable-next-line import/no-anonymous-default-export
export default {
  MODE_DEV: process.env.REACT_APP_DEV,
  SERVER: process.env.REACT_APP_SERVER,
  APP_NAME: "magnum-trade",
};
