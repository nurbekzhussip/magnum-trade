import React, { FC, useEffect } from 'react';
import { Route, Link, Switch } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import routes from './routes';
import { NAV_ITEMS, NAV_ITEMS_AUTH } from './common/constants/values';
import { PrivateRoute } from './components/enhancers/protected-route';
import { Header, Navbar } from './components/shared';
import { Auth, Home } from './components/pages';
import './i18n';
import 'antd/dist/antd.css';
import './App.scss';

const App: FC = () => {
  const isAuth = false;

  return (
    <React.Suspense
      fallback={() => (
        <div className="animated fadeIn pt-3 text-center">Loading...</div>
      )}
    >
      <Header />
      <Navbar navItems={isAuth ? NAV_ITEMS_AUTH : NAV_ITEMS} />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/auth" component={Auth} />

        {routes.map((route) => {
          return <PrivateRoute key={route.path} {...route} />;
        })}
      </Switch>
    </React.Suspense>
  );
};

export default App;
