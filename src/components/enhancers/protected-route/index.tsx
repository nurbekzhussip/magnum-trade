import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

export const PrivateRoute: React.FC<any> = (props) => {
  let { component: Component, ...rest } = props;
  const isAuth = false;
  //const isAuth = useSelector<any>((state) => state.auth.isAuth);
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuth ? <Component {...props} /> : <Redirect to="/auth" />
      }
    />
  );
};
