import React from "react";

interface ILoader {
  isShown: boolean;
}
const Loader: React.FC<ILoader> = (props: ILoader) => {
  const { isShown } = props;
  if (!isShown) {
    return null;
  }
  return <div className="loader-wrapper">LOADING</div>;
};
export default Loader;
