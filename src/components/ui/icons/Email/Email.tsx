import React, { FC } from 'react';
import { Icon, IconProps } from '../Icon';

const Email: FC<IconProps> = ({ color, width, height, ...props }) => (
  <Icon width={width} height={height} fill="none" {...props}>
    <path
      d="M14.21 2.98259V9.25514C14.21 10.3394 13.331 11.2184 12.2468 11.2184H1.9633C0.879047 11.2184 0 10.3394 0 9.25514V2.98259L6.87978 6.76664C7.02005 6.84378 7.19004 6.84378 7.33031 6.76664L14.21 2.98259ZM12.2468 0C13.3153 0 14.1844 0.853627 14.2094 1.91613L7.10504 5.8236L0.000748014 1.91609L0.00172462 1.88229C0.0441634 0.835561 0.906153 0 1.9633 0H12.2468Z"
      fill={color}
    />
  </Icon>
);

export default Email;
