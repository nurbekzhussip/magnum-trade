import React, { FC, MouseEventHandler } from 'react';
import { Button } from 'antd';
/*import openedArrowHandbook from 'asseets/images/opened-arrow.png';
import arrowHandbook from 'asseets/images/arrow-handbook.png';*/

type PropsType = {
  isOpen: boolean;
  clickHandler: MouseEventHandler<HTMLButtonElement>;
};

const Arrow: FC<PropsType> = ({ isOpen, clickHandler }: PropsType) => {
  return (
    <Button type="text" onClick={clickHandler}>
      Image
      {/*<img src={isOpen ? openedArrowHandbook : arrowHandbook} />*/}
    </Button>
  );
};

export default Arrow;
