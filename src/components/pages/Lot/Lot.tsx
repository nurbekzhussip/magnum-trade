import React, { FC } from 'react';
import { Button, Pagination, Select } from 'antd';
import { Header, Navbar } from '../../shared';

const LotList: FC = () => {
  return (
    <>
      <Header />
      <Navbar />
      <div className="container">
        <div className="--flex lots-wrapper">
          sidebar
          <div className="lots">
            pageTitle filter
            <div className="lots-row --flex">lotstates</div>
            <div className="lots-page-size --flex --justify-end">
              pageSizeData
            </div>
            list
          </div>
        </div>
      </div>

      <div>modal</div>
    </>
  );
};

export default LotList;
