import React, { FC, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Alert, Button, Input } from 'antd';
import { errorCodes } from '../../../common/constants/dictionary';
import { signIn, SignInPropsType } from '../../../ducks/auth';
import { Modal, PageTitle } from '../../shared';

import './Auth.scss';

const Auth: FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const dispatch = useDispatch();
  const [isModalVisible, setIsModalVisible] = useState(false);

  const onSubmit = (values: SignInPropsType) => {
    dispatch(signIn({ email: values.email, password: values.password }));
  };

  return (
    <div className="container sign-in-form_mb">
      <div className="sign-in__title --content-width">
        <PageTitle pageTitle="ВХОД" />
      </div>
      <div className="container">
        <div className="sign-in-form --margin-x-auto">
          {errors.email && (
            <Alert message={errorCodes.EmailRequired} type="error" />
          )}
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="sign-in-item">
              <label className="sign-in-label">Электронная почта</label>
              <Input
                className="sign-in-input"
                defaultValue="yi@magnumtech.kz"
                type="email"
                required
                {...register('email', { required: true })}
              />
            </div>
            <div className="sign-in-item">
              <label className="sign-in-label">Пароль</label>
              <Input.Password
                className="sign-in-input"
                defaultValue="test"
                required
                visibilityToggle={true}
                {...register('password', { required: true })}
              />
            </div>
            <div className="--flex --justify-center sign-in-remind_margin">
              <Button
                type="link"
                className="sign-in-remind"
                onClick={() => setIsModalVisible((prev) => true)}
              >
                Забыли пароль?
              </Button>
            </div>
            <div className="--flex --justify-center sign-in-btn">
              <Button htmlType="submit" loading={false}>
                Войти
              </Button>
            </div>
          </form>
        </div>
      </div>
      <Modal
        className="sign-in-modal"
        title="ВОССТАНОВЛЕНИЕ ДОСТУПА"
        width="772px"
        visible={isModalVisible}
        onCancel={(e) => setIsModalVisible((prev) => false)}
        footer={
          <div className="--flex --justify-center">
            <Button>ДОБАВИТЬ</Button>
          </div>
        }
      >
        <div>
          <span>Введите ваш email</span>
          <Input />
        </div>
      </Modal>
    </div>
  );
};

export default Auth;
