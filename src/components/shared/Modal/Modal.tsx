import React, { FC } from 'react';
import { Modal as AntdModal } from 'antd';

type PropsType = {
  className?: string;
  width?: string;
  title: string;
  visible: boolean;
  onCancel: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  handleOk?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  children: React.ReactNode;
  footer?: React.ReactNode;
};

const Modal: FC<PropsType> = ({
  className,
  width,
  title,
  visible,
  onCancel,
  handleOk,
  children,
  footer,
}) => {
  return (
    <AntdModal
      className={className}
      title={title}
      visible={visible}
      onOk={handleOk}
      onCancel={onCancel}
      width={width}
      footer={footer}
    >
      <div className="modal-content__container">{children}</div>
    </AntdModal>
  );
};

export default Modal;
