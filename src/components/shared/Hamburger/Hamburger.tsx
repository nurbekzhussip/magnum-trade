import React, { FC } from 'react';

type PropsType = {
  onHumburgerClickHandler: () => void;
};

const Hamburger: FC<PropsType> = ({ onHumburgerClickHandler }) => {
  return (
    <div onClick={onHumburgerClickHandler} className="hamburger hamburger--3dx">
      <div className="hamburger-box">
        <div className="hamburger-inner"></div>
      </div>
    </div>
  );
};

export default Hamburger;
