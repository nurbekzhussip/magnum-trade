import React, { FC } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Radio } from 'antd';
import { Hamburger } from '../index';

import './Navbar.scss';
import i18n from 'i18next';
import { LANGUAGES } from '../../../common/constants/values';
import { LanguageType } from '../../../models';

type PropsType = {
  navItems?: Array<{
    title: string;
    href: string;
  }>;
};

const Navbar: FC<PropsType> = ({ navItems = [] }) => {
  return (
    <div className="container">
      <div className="nav-container --menu-style --flex --justify-beetwen --align-center">
        <nav className="nav-list-container">
          <ul className="nav-list">
            {navItems.map((item, index) => (
              <li className="nav-list__item" key={index}>
                <Link to={item.href}>{item.title}</Link>
              </li>
            ))}
          </ul>
        </nav>
        <div className="--flex --justify-beetwen --align-center">
          <Radio.Group>
            {LANGUAGES.map((lang: LanguageType) => {
              return (
                <Radio.Button
                  onClick={() => i18n.changeLanguage(lang)}
                  value={lang}
                >
                  {lang.toUpperCase()}
                </Radio.Button>
              );
            })}
            {/*<Radio.Button
              style={{ marginLeft: '10px' }}
              onClick={handleRUButtonClick}
              value={LANGUAGES_ID.RU}
            >
              RU
            </Radio.Button>*/}
          </Radio.Group>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
