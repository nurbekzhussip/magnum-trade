import React, { MouseEventHandler, ReactNode } from 'react';
import { Button } from 'antd';
import { Arrow } from '../../ui/icons/Arrow';
import './PageTitle.scss';

type PropsType = {
  pageTitle: string;
  pageTitleButtonText?: string;
  buttonClickHandler?: MouseEventHandler<HTMLButtonElement>;
  arrowClickHandler?: MouseEventHandler<HTMLButtonElement>;
  content?: ReactNode;
  isOpenArrow?: boolean;
};

export function PageTitle({
  pageTitle,
  pageTitleButtonText,
  buttonClickHandler,
}: PropsType) {
  return (
    <div className=" page-title --flex --justify-beetwen --align-center">
      <div className="page-title__text">
        <span>{pageTitle}</span>
      </div>
      <div>
        {/*<Button
          className="page-title__button"
          type="text"
          onClick={buttonClickHandler}
        >
          <span>{pageTitleButtonText}</span>
        </Button>*/}
      </div>
    </div>
  );
}

export default PageTitle;
