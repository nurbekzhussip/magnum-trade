import React, { FC } from 'react';
import { LogoIcon } from '../../ui/icons';
import { PhoneIcon } from '../../ui/icons/Phone';
import { EmailIcon } from '../../ui/icons/Email';
import './Header.scss';
const Header: FC = () => {
  return (
    <div className="container">
      <div className="header-top --max-width">
        <LogoIcon width={280} height={100} />
        <div className="header-top__items --flex">
          <div className="--flex --align-center">
            <div className="header-top__tel --flex --align-center">
              <div>
                <PhoneIcon
                  width={20}
                  height={20}
                  originalWidth={16}
                  originalHeight={16}
                  color="#A42252"
                />
              </div>
              <a href="tel:+77717211900" className="header-top__text">
                +7 (771) 721 19 00
              </a>
            </div>
            <div className="header-top__mail --flex --align-center">
              <EmailIcon
                width={20}
                height={15}
                originalWidth={15}
                originalHeight={12}
                color="#A42252"
              />
              <a className="header-top__text" href="mailto:zakup@magnum.kz">
                zakup@magnum.kz
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
