import { all, takeEvery, call } from 'redux-saga/effects';
import { Record } from 'immutable';
import { createSelector } from 'reselect';
import { fetchDataFromServer } from 'utils/sagaHelper';
import { Store } from 'redux';
import { getCookie, removeCookie, setCookie } from 'utils/cookies';

import configs from 'common/constants/enviroments';
import { IReduxState } from 'common/interfaces/IReduxState';
import { IAction } from 'common/interfaces/IAction';
import { SUCCESS } from 'common/constants/types';

/**
 * Constants
 * */
export const moduleName = 'auth';
const prefix = `${configs.APP_NAME}/${moduleName}`;

export const INIT = `${prefix}/INIT`;
export const SIGN_IN = `${prefix}/SIGN_IN`;
export const SIGN_IN_REQUEST = `${prefix}/SIGN_IN_REQUEST`;
export const LOGOUT = `${prefix}/LOGOUT`;

/**
 * Reducer
 * */
export interface IAuth {
  sessionKey: string;
}
interface ISignIn extends IAction {
  payload: {
    useremail: string;
    password: string;
  };
  response: {
    token: string;
  };
}
export type SignInPropsType = {
  email: string;
  password: string;
};

const ReducerState = Record({
  sessionKey: '',
});

export default function reducer(state = new ReducerState(), action: IAction) {
  const { type } = action;
  switch (type) {
    case INIT:
      const { payload } = action;
      return state.set('sessionKey', payload.sessionKey);
    case SIGN_IN + SUCCESS:
      const { response } = action as ISignIn;
      return state.set('sessionKey', response.token);
    case LOGOUT:
      return state.clear();
    default:
      return state;
  }
}

/**
 * Selectors
 * */

export const authSelector = (state: IReduxState) => state[moduleName];
export const isAuthorizedSelector = createSelector(
  authSelector,
  (auth: IAuth) => auth.sessionKey !== '',
);

/**
 * Action Creators
 * */

export function signIn({ email, password }: SignInPropsType) {
  return {
    type: SIGN_IN_REQUEST,
    payload: { useremail: email, password },
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}

/**
 * Init logic
 */
export function init(store: Store) {
  const sessionKey = getCookie('sessionKey');
  if (sessionKey != null) {
    store.dispatch({
      type: INIT,
      payload: { sessionKey },
    });
  }
}

/**
 *Sagas
 **/

export function* signInSaga(action: ISignIn): any {
  // @ts-ignore
  yield call(fetchDataFromServer, {
    type: SIGN_IN,
    method: 'POST',
    callAPI: 'authenticate',
    payload: action.payload,
  });
}

export function* signInSuccessSaga(action: ISignIn): any {
  setCookie('sessionKey', action.response.token);
}

export function* logoutSaga(): any {
  removeCookie('sessionKey');
}

export function* saga() {
  yield all([takeEvery(SIGN_IN_REQUEST, signInSaga)]);
  yield all([takeEvery(SIGN_IN + SUCCESS, signInSuccessSaga)]);
  yield all([takeEvery(LOGOUT, logoutSaga)]);
}
