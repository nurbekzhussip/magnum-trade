import configs from "common/constants/enviroments";
import { IAction } from "common/interfaces/IAction";
import { Record } from "immutable";
import { createSelector } from "reselect";
import { IReduxState } from "common/interfaces/IReduxState";

/**
 * Constants
 * */
export const moduleName = "app";
const prefix = `${configs.APP_NAME}/${moduleName}`;
export const FETCH_START = `${prefix}/FETCH_START`;
export const FETCH_END = `${prefix}/FETCH_END`;

/**
 * Reducer
 * */
export interface IApp {
  loading: boolean;
  locale: string;
}
const ReducerState = Record({
  loading: false,
});

export default function reducer(state = new ReducerState(), action: IAction) {
  const { type } = action;
  switch (type) {
    case FETCH_START:
      return state.set("loading", true);
    case FETCH_END:
      return state.set("loading", false);
    default:
      return state;
  }
}

/**
 * Selectors
 * */
export const appSelector = (state: IReduxState) => state[moduleName];
export const isLoadingSelector = createSelector(
  appSelector,
  (state: IApp) => state.loading
);

/**
 *Sagas
 **/
export function* saga() {}
