import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import appHistory from "../appHistory";

import app from "../ducks/app";
import auth from "../ducks/auth";

const rootReducer = combineReducers({
  router: connectRouter(appHistory),
  app,
  auth,
});

export type AppStateType = ReturnType<typeof rootReducer>;

export default rootReducer;
