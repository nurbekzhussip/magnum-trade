import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import configs from 'common/constants/enviroments';
import reducer from './reducer';
import appHistory from './../appHistory';
import rootSaga from './saga';
import { init as initAuth } from '../ducks/auth';

const sagaMiddleware = createSagaMiddleware();
const enhancer = applyMiddleware(sagaMiddleware, routerMiddleware(appHistory));

const store = createStore(
  reducer,
  configs.MODE_DEV === 'true' ? composeWithDevTools(enhancer) : enhancer,
);

sagaMiddleware.run(rootSaga);
// @ts-ignore
initAuth(store);

export default store;
