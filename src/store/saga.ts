import { all } from 'redux-saga/effects';
import { saga as appSaga } from '../ducks/app';
import { saga as authSaga } from '../ducks/auth';

const saga = function* rootSaga() {
  yield all([appSaga(), authSaga()]);
}
export default saga;
