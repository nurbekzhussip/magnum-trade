import axios, { Method } from 'axios';
import { call, put } from 'redux-saga/effects';
import configs from 'common/constants/enviroments';
import { IAction } from 'common/interfaces/IAction';
import { getCookie } from './cookies';

import { START, SUCCESS, FAIL } from 'common/constants/types';
import { FETCH_END, FETCH_START } from 'ducks/app';

export const fetchDataFromServer = function* (action: IAction): any {
  const { type, method, payload, successRedirect, ...rest } = action;
  let { callAPI } = action;
  if (!callAPI) {
    return;
  }

  yield put({ ...rest, type: FETCH_START });
  yield put({ ...rest, type: type + START });

  let headers = prepareHeaders(action);
  let params: any = null;
  if (method === 'GET') {
    if (Object.entries(payload).length > 0) {
      callAPI =
        callAPI +
        '?' +
        Object.entries(payload)
          .map(([key, val]) => `${key}=${val}`)
          .join('&');
    }
  } else {
    if (headers['Content-Type'] === 'multipart/form-data;') {
      delete headers['Content-Type'];
      params = new FormData();
      Object.keys(payload).forEach((key) => {
        params.append(key, payload[key]);
      });
    } else {
      params = payload !== null ? JSON.stringify(payload) : {};
    }
  }

  try {
    const response = yield call(
      asyncCallFunc,
      configs.SERVER + callAPI,
      method,
      headers,
      params,
    );
    yield put({ ...rest, type: type + SUCCESS, response });
  } catch (error) {
    yield put({ ...rest, type: type + FAIL, error });
    // debug only
    console.log('error', error);
  } finally {
    yield put({ ...rest, type: FETCH_END });
  }
};

async function asyncCallFunc(
  callAPI: string,
  method: string,
  headers: { [key: string]: any },
  data: { [key: string]: any },
): Promise<any> {
  return axios({
    method: method as Method,
    url: callAPI,
    headers: headers,
    withCredentials: false,
    data: data,
  })
    .then((response: any) => {
      return response;
    })
    .catch((error: any) => {
      throw error;
    });
}

function prepareHeaders(action: IAction): { [key: string]: any } {
  let { authorization, headers } = action;
  headers = headers
    ? headers
    : {
        'Content-Type': 'application/json',
      };
  if (authorization) {
    headers['Authorization'] = 'Bearer ' + getCookie('sessionKey');
  }
  return headers;
}
