import React from 'react';

export const urlSearchParams = () => {
  return new URLSearchParams(window.location.search);
};
