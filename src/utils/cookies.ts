import * as Cookies from "es-cookie";

export function getCookie(key: string): string | null {
  const value = Cookies.get(key);
  if (value) {
    return value;
  } else {
    return null;
  }
}

export function setCookie(key: string, value: string): void {
  Cookies.set(key, value);
}

export function removeCookie(key: string): void {
  Cookies.remove(key);
}
