import { Auth, Home } from '../components/pages';

const routes = [
  {
    path: '/',
    component: Home,
    exact: true,
  },
  {
    path: '/lot',
    component: Home,
    exact: true,
  },
  {
    path: '/auth/sign-in',
    component: Auth,
    exact: true,
  },
];

export default routes;
