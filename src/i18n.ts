import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import common_kz from './common/locales/kz/translation.json';
import common_ru from './common/locales/ru/translation.json';

i18n.use(initReactI18next).init({
  resources: {
    kz: { translation: common_kz },
    ru: { translation: common_ru },
  },
  lng: 'ru',
  fallbackLng: 'ru',
  interpolation: {
    escapeValue: false,
  },
});
